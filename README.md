# A command-line reminder for Gitlab

## Identify yourself

    git config --global user.name "user"
    git config --global user.email "user@email.com"

Omit `--global` to set it up just for the current repository.

## Check the SSH connection:

    ssh -T git@gitlab.com
Should return:*"Welcome to GitLab, @user!"*

## Create and push a new repository through SSH :

Replace "new_repository" by your new repository name :

    mkdir new_repository
    cd new_repository
    git init
    touch README.md
    git add .
    git commit -m "initial commit"
    git remote add new_repository git@gitlab.com:user/new-repository.git #adds the ssh repository address to new_repository/.git/config
    git push new-repository --all #Pushes all branches
    git push --set-upstream new_repository master #Sets branch master as upstream

## Create a new branch from an existing one (master for example):

    git checkout master
    git checkout -b newbranch
    git push --all
    git push --set-upstream origin newbranch #sends new branch to gitlab and sets it at upstream  

## Checkout on a given commit:

    git checkout commitSHA

## Merge dev into master:

    git checkout master
    git merge dev

## Solve conflicts using command line :

    git status

Then open the file with Atom and merge.

To mark as solved, add the file with `git add .` Beware : in this case the conflicts appear in the file, you can correct it afterwise.

Source : https://docs.github.com/en/github/collaborating-with-issues-and-pull-requests/resolving-a-merge-conflict-using-the-command-line

## Enable LFS:

    git lfs install
    git lfs track "filetype" #Example : git lfs track "*.sql"
    git add .gitattributes

Then add, commit and push as usually.

To clone faster :

    git lfs clone

[Smudge option](https://confluence.atlassian.com/bitbucketserverkb/files-larger-than-4-gb-are-not-correctly-handled-on-windows-935385144.html)

## Others:

    git commit -a #add and remove files that were added or removed from the local repo.
    git reset --hard
    git rm
